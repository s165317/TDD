package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import core.ResizingArrayQueueOfStrings;


public class ResizingArrayQueueOfStringsTest {

	
	
	private ResizingArrayQueueOfStrings arr;
	
	@Before 
	public void createEmptyarray() {
		arr = new ResizingArrayQueueOfStrings();
	}
	@Test
	public void testIsEmpty() {
		assertTrue("Should be empty",true);
	}
	
	@Test
	public void testIsEmpty1() {
		arr.enqueue("man");
		assertTrue("Should be empty",true);
	}
	@Test
	public void testSize() {
		arr.size();
		
	}
	@Test
	public void testSize1() {
		arr.enqueue("genius");
		arr.enqueue("Long");
		arr.enqueue("BullShit");
		assertEquals(arr.size(),3);
		
	}

	@Test
	public void testEnqueue() {
		arr.enqueue("man");
		arr.enqueue("strong");
		assertEquals(arr.size(),2);
		
		
	}

	@Test
	public void testDequeue() {
		arr.enqueue("man");
		arr.enqueue("strong");
		arr.dequeue();
		arr.dequeue();
	}
	
	@Test
	public void testDequeue1() {
		arr.dequeue();
	}
	@Test
	public void testDequeue2() {
		arr.enqueue("man");
		arr.enqueue("strong");
		arr.dequeue();
	}

	@Test
	public void testPeek() {
		arr.enqueue("genius");
		arr.enqueue("Long");
		arr.enqueue("Qin");
		arr.peek();
		
	}
	
	@Test
	public void testPeek1() {
		arr.peek();
		
	}
		
}
