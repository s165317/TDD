package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import core.MyDate;

public class MyDateTest {
	
		
	private MyDate ex;
	private MyDate ex1;
	private MyDate ex2;


	@Before
	public void createEmptyDate() {
		ex = new MyDate(2, 14, 2017); 
		ex1 = new MyDate(2,1,2017);
		ex2 = new MyDate(1,1,2016);
		

	}
 // 1-6 aims to fulfill the most part of next method 
	@Test
	public void testNext() {
		ex.next();
		}
	@Test
	public void testNext1() {
		ex1.next();
		}
	@Test
	public void testNext2() {
		ex2.next();
		}
	@Test
	public void testNext3() {
		MyDate ex3 = new MyDate(13,1,2016);
		ex3.next();
		}
	@Test
	public void testNext4() {
		MyDate ex4 = new MyDate(12,40,2016);
		ex4.next();
		}
	@Test
	public void testNext5() {
		MyDate ex5 = new MyDate(2,29,2004);
		ex5.next();
		}
	
	@Test
	public void testNext6() {
		MyDate ex9 = new MyDate(1,1,2016);
		ex9.next();
	}
	@Test
	public void testNext8() {MyDate ex7 = new MyDate(1,0,2004);} // 8 to 11 is to fulfill the requirement of isValid which is a private method
	@Test
	public void testNext9() {MyDate ex8 = new MyDate(13,0,2004);}
	@Test
	public void testNext10() {MyDate exx = new MyDate(0,0,2004);}
	@Test
	public void testNext11() {MyDate exx1 = new MyDate(0,3,2004);}
	@Test
	public void testNext12() {MyDate exx2 = new MyDate(1,31,2004); exx2.next();}  // the second command for the Next
	@Test
	public void testNext13() {MyDate exx3 = new MyDate(12,31,2004);exx3.next();} //the second command for the Next
	@Test 
	/* there are two branches for both isAftser and isBefore, which is a smaller than b and a bigger than b,respectively.*/
	public void testIsAfter() {
		MyDate ex9 = new MyDate(1,1,2016);
		ex9.isAfter(ex1);
	}
	@Test
	public void testIsAfter1() {
		MyDate ex10 = new MyDate(1,2,2016);
		MyDate ex9 = new MyDate(1,1,2016);
		ex10.isAfter(ex9);
	}

	@Test
	public void testIsBefore() {
		MyDate ex10 = new MyDate(1,2,2016);
		MyDate ex9 = new MyDate(1,1,2016);
		ex10.isBefore(ex9);
	}
	@Test
	public void testIsBefore1() {
		MyDate ex10 = new MyDate(1,2,2016);
		MyDate ex9 = new MyDate(1,1,2016);
		ex9.isBefore(ex10);
	}

	@Test
	public void testCompareTo() {
		MyDate ex11 = new MyDate(3,5,2016);
		MyDate ex12 = new MyDate(4,16,2016);
		ex12.compareTo(ex11);
	}

	@Test 
	public void testToString() {
		ex1.toString();	}

}
