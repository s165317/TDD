package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import core.FixedCapacityStackOfStrings;

public class FixedCapacityStackOfStringsTest {

	
	private  FixedCapacityStackOfStrings stack;
	
	@Before
	public void createEmptyStack() {
		stack = new FixedCapacityStackOfStrings(5);
	}
	
	
	@Test
	public void testIsEmpty() {
		
		assertTrue("Should be empty",stack.isEmpty());
	}
	@Test
	public void testIsEmpty1() {
		stack.push("Apple");
		assertTrue("Should be empty",stack.isEmpty());
	}



	@Test
	public void testIsFull() {
		
		stack.push("Apple");
		stack.push("Banana");
		stack.push("Clementine");
		stack.push("Date");
		stack.push("Elderberry");
		assertTrue ("Should be full",stack.isFull());
	}
	@Test
	public void testIsFull1() {
		
		
		stack.push("Banana");
		stack.push("Clementine");
		stack.push("Date");
		stack.push("Elderberry");
		assertTrue ("Should be full",stack.isFull());
	}
	@Test
	public void testSize() {
		
		assertEquals(0,stack.size());
		stack.push("Apple");
		assertEquals(1,stack.size());
		stack.pop();
		assertEquals(0,stack.size());
	}

	@Test (expected = IndexOutOfBoundsException.class)
	public void testPushAlreadyFull() {
		stack.push("Appple");
		stack.push("Banana");
		stack.push("Clementine");
		stack.push("Date");
		stack.push("sdas");
		stack.push("Fig");
	}

	@Test (expected = IndexOutOfBoundsException.class)
	public void testPop() {
		stack.pop();
	}

	@Test
	public void testPeek() {
		stack.push("Apple");
		stack.push("Banana");
		assertEquals("Banana",stack.peek());
		stack.pop();
		assertEquals("Apple",stack.peek());
	}
	@Test (timeout = 5000)
	public void testPerformance() {
		FixedCapacityStackOfStrings  stackPerf = new FixedCapacityStackOfStrings(1000000);
		for (int i=1;i<1000000;i++)
			stackPerf.push("Element" + i);
	}
	
}
