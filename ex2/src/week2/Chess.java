package week2;

interface Piece {
	/**
	 * This method does not have to check the validity of the position
	 */
	public void setArbitraryPosition(Position currentPosition); // on abstract class 

	/**
	 * This method checks if the position is a valid position
	 */
	public boolean isValidPosition(Position newPosition); // on lowest type
	
	/**
	 * This method moves the piece
	 * @return true if the piece has been moved correctly, false otherwise
	 */
	public boolean move(Position newPosition); // on abstract class
}

class Position {
	private int x, y;
	public Position(char x, int y) {
		this.x = x - 'a';
		this.y = y - 1;
	}
	public boolean isOnBoard() {
		return (x >= 0 && x <= 7 && y >= 0 && y <= 7);
	}
	public int x() {
		return x;
	}
	public int y() {
		return y;
	}
	@Override
	public String toString() {
		return "("+x+","+y+")";
	}
}

class Player {
	private boolean isWhite;
	public Player(String n) { }
	public void setColorWhite(boolean w) {
		this.isWhite = w;
	}
	public boolean isWhite() {
		return isWhite;
	}
}

abstract class AbstractPiece implements Piece {
	protected Position current;
	protected Player owner;
	public AbstractPiece(Player owner) {
		this.owner = owner;
	}
	public void setArbitraryPosition(char x, int y) {
		setArbitraryPosition(new Position(x, y));
	}
	@Override
	public void setArbitraryPosition(Position currentPosition) {
		if (currentPosition.isOnBoard()) {
			this.current = currentPosition;
		}
	}
	@Override
	public boolean move(Position newPosition) {
		if (isValidPosition(newPosition)) {
			this.current = newPosition;
			return true;
		}
		return false;
	}
}

class King extends AbstractPiece {
	public King(Player owner) {
		super(owner);
	}
	
	public boolean isValidPosition(Position newPosition) {
		if (newPosition.isOnBoard()) {
			int sumDiffs = Math.abs(current.x() - newPosition.x()) + Math.abs(current.y() - newPosition.y());
			if (sumDiffs == 1 || sumDiffs == 2) {
				return true;
			}
		}
		return false;
	}
}

class Rook extends AbstractPiece {
	public Rook(Player owner) {
		super(owner);
	}
	@Override
	public boolean isValidPosition(Position newPosition) {
		if (newPosition.isOnBoard()) {
			int diffX = Math.abs(current.x() - newPosition.x());
			int diffY = Math.abs(current.y() - newPosition.y());
			if ((diffX == 0 && diffY > 0) || (diffX > 0 && diffY == 0)) {
				return true;
			}
		}
		return false;
	}	
}

class Bishop extends AbstractPiece {
	public Bishop(Player owner) {
		super(owner);
	}
	@Override
	public boolean isValidPosition(Position newPosition) {
		if (newPosition.isOnBoard()) {
			int diffX = Math.abs(current.x() - newPosition.x());
			int diffY = Math.abs(current.y() - newPosition.y());
			if (diffX == diffY && diffX > 0) {
				return true;
			}
		}
		return false;
	}
}

class Knight extends AbstractPiece {
	public Knight(Player owner) {
		super(owner);
	}
	@Override
	public boolean isValidPosition(Position newPosition) {
		if (newPosition.isOnBoard()) {
			int diffX = Math.abs(current.x() - newPosition.x());
			int diffY = Math.abs(current.y() - newPosition.y());
			if ((diffX == 1 && diffY == 2) || (diffX == 2 && diffY == 1)) {
				return true;
			}
		}
		return false;
	}
}

class Pawn extends AbstractPiece {
	public Pawn(Player owner) {
		super(owner);
	}
	@Override
	public boolean isValidPosition(Position newPosition) {
		if (newPosition.isOnBoard()) {
			int diffX = current.x() - newPosition.x();
			int diffY = current.y() - newPosition.y();
			if (diffX != 0) {
				return false;
			} else {
				if (owner.isWhite()) {
					if (current.y() == 1) {
						if (diffY == -1 || diffY == -2) {
							return true;
						} else {
							return false;
						}
					} else {
						return (diffY == -1);
					}
				} else {
					if (current.y() == 6) {
						if (diffY == 1 || diffY == 2) {
							return true;
						} else {
							return false;
						}
					} else {
						return (diffY == 1);
					}
				}
			}
		}
		return false;
	}
}

public class Chess {
	
	public static void main(String[] args) {
		Player p1 = new Player("White player");
		p1.setColorWhite(true);
		Player p2 = new Player("Black player");
		p2.setColorWhite(false);
		
		System.out.println("Testing kings:");
		Piece whiteKing = new King(p1);
		whiteKing.setArbitraryPosition(new Position('f', 5));
		test(whiteKing, 'a', 1, false);
		test(whiteKing, 'f', 4, true);
		
		System.out.println("Testing rooks:");
		Rook blackRook = new Rook(p2);
		blackRook.setArbitraryPosition('d', 5);
		test(blackRook, 'h', 5, true);
		test(blackRook, 'h', 1, false);
		test(blackRook, 'd', 9, false);
		
		System.out.println("Testing bishops:");
		Piece whiteBishop = new Bishop(p1);
		whiteBishop.setArbitraryPosition(new Position('d', 5));
		test(whiteBishop, 'b', 2, false);
		test(whiteBishop, 'a', 8, true);
		
		System.out.println("Testing knigts:");
		Knight blackKnight = new Knight(p2);
		blackKnight.setArbitraryPosition('d', 4);
		test(blackKnight, 'e', 6, true);
		test(blackKnight, 'f', 6, false);
		test(blackKnight, 'c', 2, true);
		test(blackKnight, 'i', 8, false);
		
		System.out.println("Testing pawns:");
		Pawn whitePawn = new Pawn(p1);
		Pawn blackPawn = new Pawn(p2);
		blackPawn.setArbitraryPosition('b', 4);
		test(blackPawn, 'b', 3, true);
		test(blackPawn, 'b', 5, false);
		whitePawn.setArbitraryPosition('f', 2);
		test(whitePawn, 'f', 3, true);
		test(whitePawn, 'f', 4, true);
		blackPawn.setArbitraryPosition('g', 5);
		test(blackPawn, 'g', 4, true);
		test(blackPawn, 'g', 3, false);
		whitePawn.setArbitraryPosition('e', 7);
		test(whitePawn, 'd', 8, false);
		test(whitePawn, 'f', 8, false);
	}
	
	public static void test(Piece p, char x, int y, boolean valid) {
		if (p.isValidPosition(new Position(x, y)) == valid) {
			System.out.println("  > Test passed!");
		} else {
			System.out.println("  X Test NOT passed!");
		}
	}
}