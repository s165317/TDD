package test;

public class StudentController {
	   private student model;
	   private studentview view;

	   public StudentController(student model, studentview view){
	      super();
		  this.model = model;
	      this.view = view;
	   }
	   
	   public void updateView(){				
		      view.printStudentDetail(model.getName(), model.getRollNo());
		   }
	  
	   public void setStudentName(String name){
	      model.setName(name);		
	   }

	   public String getStudentName(){
	      return model.getName();		
	   }

	   public void setStudentRollNo(String rollNo){
	      model.setRollNo(rollNo);		
	   }

	   public String getStudentRollNo(){
	      return model.getRollNo();		
	   }

	

}
