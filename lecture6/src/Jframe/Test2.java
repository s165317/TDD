package Jframe;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;


class simpleButtonListener implements ActionListener{
	private int i =0;
	
	
	@Override
	public void actionPerformed(ActionEvent e) {
		i++;
		System.out.println(e.getActionCommand() + " - " + i);
	}
} 

public class Test2 {

	public static void main(String[] args) {
		
		JFrame frame = new JFrame("Chrispy");
		
		JButton b1 =new JButton("hit me!");
		b1.setActionCommand("long");
		
		b1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e){
				
			}
		});
		/*JButton b2 = new JButton("hit me please");
		b2.setActionCommand("Qin");
		
		simpleButtonListener l = new simpleButtonListener();
		b1.addActionListener(l);
		b2.addActionListener(l);*/
		
		frame.setLayout(new FlowLayout());
		frame.add(b1);
		//frame.add(b2);
		frame.pack();
		
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		
	}

}
