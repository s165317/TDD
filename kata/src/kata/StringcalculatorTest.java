package kata;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.Rule;
import org.junit.jupiter.api.Test;
import org.junit.rules.ExpectedException;

class StringcalculatorTest {

	@Test
	void test() throws Exception {
		stringcalculator test = new stringcalculator();	
		assertEquals(0, test.add("") );
	}

	@Test
	void test1() throws Exception {
		stringcalculator test = new stringcalculator();
		assertEquals(3, test.add("2,1") );
	}
	@Test
	void test2() throws Exception {
		stringcalculator test = new stringcalculator();
		assertEquals(4, test.add("4") );
	}
	@Test
	void test3() throws Exception {
		stringcalculator test = new stringcalculator();
		assertEquals(28, test.add("2,2,7,8,9") );
	}
	@Test
	void test4() throws Exception {
		stringcalculator test = new stringcalculator();
		assertEquals(28, test.add("2,2\n7,8\n9") );
	}
	@Test
	void test5() throws Exception {
		stringcalculator test = new stringcalculator();
		assertEquals(28, test.add("//;\n2;2;7;8;9") );
	}
	@Test
	void test6() throws Exception {
		stringcalculator test = new stringcalculator();
		assertEquals(34, test.add("2;4\n2;2;7;8;9") );
	}
	@Test
	void test7() throws Exception {
		stringcalculator test = new stringcalculator();
		assertEquals(28, test.add("//&\n2&2&7&8&9") );
	}
	/*@Rule
	 public final ExpectedException exception = ExpectedException.none();*/
	
/*@Test
	void test8() throws Exception {
		stringcalculator test = new stringcalculator();
		Object a=exception.expectMessage("negative not allowed!")+"[-2,-2,-8]";
		assertEquals(a, test.add("//&\n-2&-2&7&-8&9") );
	}

*/	
	
}
