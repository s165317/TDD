package kata;
import java.util.*;
import java.util.Scanner;

import org.junit.platform.commons.util.ExceptionUtils;

public class stringcalculator {

	public static void main(String[] args) {
	}
	
	public static int add(String number1) throws Exception{
		int default_output = 0;
		int sum=0;
		List<Integer> negatives = new ArrayList<Integer>();
		if (number1.isEmpty()) {
			return default_output;	
		}else if(!number1.isEmpty()){
				if(number1.startsWith("//")) {
					String delimiter = number1.substring(2, 3);
					number1 = number1.substring(4);
					String[] numbersd = number1.split(delimiter);
					int numberscount1=numbersd.length;
					for(int i=0;i<numberscount1;i++){
					int n = Integer.parseInt(numbersd[i]);
						if (n<0) {
							for(int j=i;j<numberscount1;j++) {
								if(Integer.parseInt(numbersd[j])<0) {
								negatives.add(Integer.parseInt(numbersd[j]));
								}
							}
				 		throw new Exception("find negative!" + negatives);
						}else {
				 		sum += n;			
						}
					}
				}else{
					String[] numbers = number1.split(";|,|\\\n");
					int numberscount2=numbers.length;
					for(int i=0;i<numberscount2;i++) {
					int n = Integer.parseInt(numbers[i]);
						if (n<0) {
							for(int j=i;j<numberscount2;j++) {
								if(Integer.parseInt(numbers[j])<0) {
									negatives.add(Integer.parseInt(numbers[j]));
								}
							}
					 	throw new Exception("negative not allowed!" + negatives);
					 	}else {
					 	sum += n;	
					 	}
				}
			}
		}
		return sum;
	}
	
	
}
