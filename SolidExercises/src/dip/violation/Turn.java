package dip.violation;

public interface Turn {
	void turnOn();
	void turnOff();
}
