package dip.violation.test;

import dip.violation.Button;

import dip.violation.Lamp;



public class TestProgram {

	public static void main(String[] args)
	{
		Lamp lamp = new Lamp();
		Button button = new Button(lamp);
		button.push();
		button.push();
	}
		
	  
}
