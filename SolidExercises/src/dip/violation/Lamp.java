package dip.violation;


public class Lamp implements Turn {
	@Override
	 public void turnOn()
	 {
		System.out.println("Lamp is ON");
	 }
	 @Override 
	 public void turnOff()
	 {
		 System.out.println("Lamp is OFF");
	}
}
