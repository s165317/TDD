package isp.violation;

public class ElectricCar implements ECar
{
    private int speed ;
 
    public int getSpeed() {
 
		return speed;
	}

	public void setSpeed(int speed) {
		this.speed = speed;
	}

	@Override
	public void accelerate() {
        this.speed++;
    }
 
   
}
