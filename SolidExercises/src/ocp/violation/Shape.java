package ocp.violation;

public interface Shape {
	double getArea();
}
