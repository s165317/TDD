package ocp.violation;

public class AreaCalculator
{
	
	public AreaCalculator() {
		super();
	}

	public double calculateArea(Object[] shapes)
	{
	    double area = 0;
	    
	    for (Object shape : shapes)
	    {
		    	
		    	if (shape instanceof Rectangle)
		    	{
		          Rectangle rectangle = (Rectangle) shape;
		          area += rectangle.getArea();

		
	    } else {
	    	Circle circle = (Circle) shape;
	    	area += circle.getArea();
	    }
	    }
		return area;
}}
