package ocp.violation;

public class Circle {
	
	private double radius;

	public double getRadius() {
		return radius;
	}

	public void setRadius(double radius) {
		this.radius = radius;
	}
	public double getArea() {
        return (Math.pow(this.getRadius(),2) * Math.PI);
   
	}
	
	public Circle(double radius) {
		super();
		setRadius(radius);
	}

	
}
