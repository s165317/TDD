package ocp.violation.test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import ocp.violation.AreaCalculator;
import ocp.violation.Circle;
import ocp.violation.Rectangle;


class AreaCalculatorTest {

	@Test
	void testCaclulateArea() {
		AreaCalculator calc = new AreaCalculator();
		Circle circle = new Circle(2.0);
		Rectangle rectangle = new Rectangle(3.0,4.0);
		Object [] shapes = new Object []{circle, rectangle};
		double area = calc.calculateArea(shapes);
		assertEquals(12.0 + Math.pow(2.0,2)*Math.PI,area);
	}

}
