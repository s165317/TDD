package ocp.violation;

public class Rectangle implements Shape {
    public double width;
    public double height;
    
	public double getWidth() {
		return width;
	}
	public void setWidth(double width) {
		this.width = width;
	}
	public double getHeight() {
		return height;
	}
	public void setHeight(double height) {
		this.height = height;
	}
	
	public Rectangle(double width, double height) {
		super();
		this.width = width;
		this.height = height;
	}
	public double getArea() {
		return (double) (this.getWidth()*this.getHeight());  
	}
    
}