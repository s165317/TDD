package lsp.violation.test;





import lsp.violation.Bicycle;
import lsp.violation.Car;
import lsp.violation.TransportationDevice;



public class TestProgram {

	public static void main(String[] args)
	{
		TransportationDevice car = new Car();
		TransportationDevice bike = new Bicycle();
		Object[] devices = new Object [] {car, bike};
		
		for (Object device : devices)
	    {
			if (device instanceof Car) {
		    	Car.startEngine();
			}
		
	    }
	}
		
	  
}


