package lecture;

import java.util.ArrayList;

//1.define class
class C {
	private String s;
	
	public C(String s) {
		this.s=s;
	}
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof C) {
			return s.equals(((C)obj).s);
		}
		return false;
	}
}

public class interatorArrayList {

	public static void main(String[] args) {
		ArrayList<C> list = new ArrayList<C>();
		C c = new C("test");
		list.add(c);
		list.add(c);
		System.out.println(list.size());
		
		boolean contains = list.contains(new C("test"));
		System.out.println(contains);
		
		list.remove(new C("test"));
		System.out.println(list.size());
		
		list.remove(1);
		System.out.println(list.size());

	}

}
