package lecture2;
class Employee{
	private String name;
	private int salary;
	
	public Employee(String name, int salary) {
		this.name= name;
		this.salary = salary;
	}
	public void print() {
		System.out.println(name + " earns" + salary);
	}
	public Employee(String name) {
		this(name,-1);
		System.out.println("Employee(String)");
	}
	public Employee() {
		this("Unknown"); 
		System.out.println("Employee()");
	}
	
// 由下到上的嵌套
	// print 出 unknown earns -1
		
}
public class ponitt1 {

	public static void main(String[] args) {
		
	}

}
