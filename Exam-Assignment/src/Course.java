public class Course {
	String title;
	int courseNumber;
	String description;
	String learningGoal;
	int ECTS;
	Course[] prerequest;
	Teacher[] responsibleTeachers;
	TeachingAssitent[] teachingAssistents;
	String courseLevel;
	Date schedule;
}
