#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@tag
Feature: KATA calculator feature

  @tag1
  Scenario: Adding an empty string
    Given a add calculator
    And and empty string ""
    Then System return default integer 0

  @tag2
  Scenario: Adding only one number
  Given a add calculator
    And and  string  "2"
    Then System return another integer 2
    
    
    @tag2
  Scenario: Adding two numbers with ,
  Given a add calculator
    And give a string "2,3"
    Then calculator should return another integer 5
 