package com.tokyo.dtu.kata.kata;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features="features", 
				 snippets=SnippetType.CAMELCASE)
public class CucumberTest {

}
