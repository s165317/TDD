package com.tokyo.dtu.kata.kata;

import cucumber.api.java.en.Given;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class stepdefinition {
	stringcalculator test = new stringcalculator();
	String arg1;
	
	
	@Given("^a add calculator$")
	public void a_add_calculator() throws Throwable {
		stringcalculator test = new stringcalculator();
	}

	@Given("^and empty string \"([^\"]*)\"$")
	public void and_empty_string(String arg1) throws Throwable {
	    this.arg1=arg1;
	}

	@Then("^System return default integer (\\d+)$")
	public void system_return_default_integer(String arg1) throws Throwable {
		assertEquals(0, test.add(arg1) );
	}

	@Given("^and  string  \"([^\"]*)\"$")
	public void and_string(String arg1) throws Throwable {
	    this.arg1=arg1;
	}

	@Then("^System return another integer (\\d+)$")
	public void system_return_another_integer(String arg1) throws Throwable {
	    assertEquals(2, test.add(arg1) );
	}
	
	@Given("^give a string \"([^\"]*)\"$")
	public void give_a_string(String arg1) throws Throwable {
	   this.arg1=arg1;
	}

	@Then("^calculator should return another integer (\\d+)$")
	public void calculator_should_return_another_integer(String arg1) throws Throwable {
		assertEquals(5, test.add(arg1) );
	}

	
}
