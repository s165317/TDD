package rejsekort;

public class ResponseObject {
	int error;
	String errorMessage;
	public int getError() {
		return error;
	}
	public void setError(int error) {
		this.error = error;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	public ResponseObject(int error, String errorMessage) {
		super();
		this.error = error;
		this.errorMessage = errorMessage;
	}
}
