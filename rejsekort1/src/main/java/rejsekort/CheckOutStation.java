package rejsekort;

public class CheckOutStation extends Station {
	


	private static final int TRANSPORATION_FARE = 25;
    String name;
	public CheckOutStation(String name) {
		super(name);
	
	}

	public ResponseObject checkout(TravelCard card) {
		
		ResponseObject response;
		
		if (card.isCheckedIn()) {
				calculateFare(card);
				card.setCheckedIn(false);
				response = new ResponseObject(250, "Checked out");
			
		} else {
			
			response = new ResponseObject(330, "Not Checked in");
		
		}
		return response;
	}

	private void calculateFare(TravelCard card) {
		card.setBalance(card.getBalance()-TRANSPORATION_FARE);
		
	}

	

}
