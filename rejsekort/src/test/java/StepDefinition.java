import static org.junit.Assert.assertEquals;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class StepDefinition {

	TravelCard card = new TravelCard();
	CheckInStation inStation;
	CheckOutStation outStation;
	ResponseObject response;
	
	@Given("^a travel card with a balance of (\\d+)$")
	public void a_travel_card_with_a_balance_of(int balance) {

	    card.setBalance(balance);
	}

	@Given("^check-in status is (true|false)$")
	public void check_in_status_is_false(boolean isCheckedIn) {
	   card.setCheckedIn(isCheckedIn);
	   
	}

	@Given("^a check-in automaton at \"([^\"]*)\"$")
	public void a_check_in_automaton_at(String station)  {
	    inStation = new CheckInStation(station);
	}

	@When("^check-in$")
	public void check_in()  {
	    response = inStation.checkin(card);
	}

	@Then("^automaton displays message that the card is checked-in$")
	public void automaton_displays_message_that_the_card_is_checked_in() {
		assertEquals(response.getErrorMessage(),"Checked-in");
	}
	
	@Then("^automaton displays message that the card is already checked-in$")
	public void automaton_displays_message_that_the_card_is_already_checked_in() {
		assertEquals(response.getErrorMessage(),"Already checked-in");
	}
	
	@Then("^automaton displays message that the balance is too low$")
	public void automaton_displays_message_that_the_balance_is_too_low()  {
		assertEquals(response.getErrorMessage(),"Balance too low");
	}
	

	@Given("^a check-out automaton at \"([^\"]*)\"$")
	public void a_check_out_automaton_at(String station) {
		 outStation = new CheckOutStation(station);
	}

	@When("^check-out$")
	public void check_out() {
		response = outStation.checkout(card);
	}

	@Then("^automaton displays message that the card is checked-out$")
	public void automaton_displays_message_that_the_card_is_checked_out()  {
		assertEquals(response.getErrorMessage(),"Checked out");
	}
	
	@Then("^automaton displays message that the card is not checked-in$")
	public void automaton_displays_message_that_the_card_is_not_checked_in() throws Throwable {
		assertEquals(response.getErrorMessage(),"Not checked-in");
	}


	@Then("^travel card has new balance (\\d+)$")
	public void travel_card_has_new_balance(int balance){
		assertEquals(card.getBalance(),balance);
	}
	
}
