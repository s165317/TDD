
public class CheckOutStation extends Station {
	
	private static final int TRANSPORTATION_FARE = 25;
	

	public CheckOutStation(String name) {
		super(name);
	}

	public ResponseObject checkout(TravelCard card) {
		
		ResponseObject response;
		
		if (card.isCheckedIn()) {
				
				calculateFare(card);
				card.setCheckedIn(false);
				response = new ResponseObject(250, "Checked out");
			
		} else {
			
			response = new ResponseObject(330, "Not checked-in");
		
		}
		return response;
	}

	private void calculateFare(TravelCard card) {
		card.setBalance(card.getBalance()-TRANSPORTATION_FARE);
		
	}

	

}
