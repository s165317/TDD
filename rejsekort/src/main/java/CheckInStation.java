
public class CheckInStation extends Station {
	


	public CheckInStation(String name) {
		super(name);
	
	}

	public ResponseObject checkin(TravelCard card) {
		
		ResponseObject response;
		
		if (!card.isCheckedIn()) {
			if (hasEnoughBalance(card)) {
				card.setCheckedIn(true);
				response = new ResponseObject(200, "Checked-in");
			} else {
				
				response = new ResponseObject(320, "Balance too low");
			
			}
		} else {
			
			response = new ResponseObject(300, "Already checked-in");
		
		}
		return response;
	}

	private boolean hasEnoughBalance(TravelCard card) {
		
		return card.getBalance()>=80;
	}

}
