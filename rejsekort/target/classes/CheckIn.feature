#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@tag
Feature: Checking in with travel card

  @tag1
  Scenario: Successful check-in
    Given a travel card with a balance of 100
    And check-in status is false
    And a check-in automaton at "Norreport St"
    When check-in
    Then automaton displays message that the card is checked-in

 Scenario: Already checked-in
    Given a travel card with a balance of 100
    And check-in status is true
    And a check-in automaton at "Norreport St"
    When check-in
    Then automaton displays message that the card is already checked-in
    
 Scenario: Card has not enough balance
    Given a travel card with a balance of 20
    And check-in status is false
    And a check-in automaton at "Norreport St"
    When check-in
    Then automaton displays message that the balance is too low